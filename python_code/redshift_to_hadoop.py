from pyspark import SparkContext
from pyspark.sql import SQLContext, Row
sc = SparkContext(appName = "test")
sqlContext = SQLContext(sc)
import json




df = sqlContext.read.format("jdbc").options(
    url="jdbc:postgresql://demo-ujjwal.c4ofvsnvkbje.us-west-2.redshift.amazonaws.com:5439/myred?user=master&password=Redshift123",
    dbtable = "bunny_redshift").load()

'''
df = sql_context.read \
    .format("com.databricks.spark.redshift") \
    .option("url", "jdbc:redshift://demo-ujjwal.c4ofvsnvkbje.us-west-2.redshift.amazonaws.com:5439/myred?user=master&password=Redshift123") \
    .option("dbtable", "my_table") \
    .option("tempdir", "s3n://path/for/temp/data") \
    .load()

'''
df.write.format("json").save("/redshift/redshift1")
