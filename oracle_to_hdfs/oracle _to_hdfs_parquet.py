from pyspark import SparkContext
from pyspark.sql import SQLContext, Row
import json
sc = SparkContext(appName = "test")
sqlContext = SQLContext(sc)


df = sqlContext.read.format("jdbc").options(
    url="jdbc:oracle:thin:ujjwal/ujjwal@192.168.1.171:1527:xe",
    driver =  "oracle.jdbc.driver.OracleDriver",
    dbtable = "MOCK_DATA_D1").load()

df.show()
df.write.format("json").save("/oracle")
