from pyspark import SparkContext
from pyspark.sql import SQLContext, Row
sc = SparkContext(appName = "test")
sqlContext = SQLContext(sc)
import json


df=sqlContext.read.json("/america/*.json")

df.write.jdbc(url="jdbc:redshift://demo-ujjwal.c4ofvsnvkbje.us-west-2.redshift.amazonaws.com:5439/myred?user=master&password=Redshift123", table = "bunny_redshift", mode="append")

'''
# Read data from a table
df = sql_context.read \
    .format("com.databricks.spark.redshift") \
    .option("url", "jdbc:redshift://redshift.cx7rlycokw7k.us-east-1.redshift.amazonaws.com:5439/cloudcdc?user=admin&password=Admin123") \
    .option("dbtable", "my_table") \
    .option("tempdir", "s3n://path/for/temp/data") \
    .load()



# Read data from a query
df = sql_context.read \
    .format("com.databricks.spark.redshift") \
    .option("url", "jdbc:redshift://redshifthost:5439/database?user=username&password=pass") \
    .option("query", "select x, count(*) my_table group by x") \
    .option("tempdir", "s3n://path/for/temp/data") \
    .load()


# Write back to a table
df.write \
  .format("com.databricks.spark.redshift") \
  .option("url", "jdbc:redshift://demo-ujjwal.c4ofvsnvkbje.us-west-2.redshift.amazonaws.com:5439/myred?user=master&password=Redshift123") \
  .option("dbtable", "new_hadoop_table_bunny") \
  .option("tempdir", "s3n://us-west-2.amazonaws.com/bunny-testing/temp/") \
  .mode("error") \
  .save()



# Using IAM Role based authentication
df.write \
  .format("com.databricks.spark.redshift") \
  .option("url", "jdbc:redshift://redshifthost:5439/database?user=username&password=pass") \
  .option("dbtable", "my_table_copy") \
  .option("tempdir", "s3n://path/for/temp/data") \
  .option("aws_iam_role", "arn:aws:iam::123456789000:role/redshift_iam_role") \
  .mode("error") \
  .save()'''
